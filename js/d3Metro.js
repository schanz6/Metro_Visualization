
var svg = d3.select("svg");


var tooltip = d3.select("body")
    .append("div")
    .style("position", "absolute")
    .style("z-index", "10")
    .style("visibility", "hidden")
    .style("color", "white")
    .style("background", "black")
    .style("border-color", "black")
    .style("border-radius", "10px")
    .style("border-style", "solid");


//runFunction();
var time =setInterval(updateMetro,10000);
//setMetroTrains(MetroTest);
updateMetro();

function updateMetro()
{
	$.ajax({
	   url: 'http://api.wmata.com/StationPrediction.svc/json/GetPrediction/ALL?api_key=xenpbhun9vy4wkavqdxmrgt2',
	   data: {
	      format: 'json'
	   },
	   error: function() {
	      console.log("error");
	   },
	   dataType: 'jsonp',
	   success: function(data) {
	      
	       setMetroTrains(data.Trains);

	   },
	   type: 'GET'
	});

}

function setMetroTrains(metroData)
{

	 var data = getTrains(metroData)
	 render(data);
	       
	       
	       for (var i = 0; i < data.length; i++) {
	       		//console.log(" train ")
	       	//console.log(data[i].Train.DestinationName+" : "+data[i].Train.LocationName+" "+data[i].Train.Min);
	       		
	       }

}



// draws circels on map
function render(data)
{
	svg.selectAll("g").remove();

	var groups = svg.selectAll("g")
	    .data(data)
	    .enter()
	    .append("g");

	groups.attr("transform", function(d, i) {
			var x = d.Station.x+d.Direction;
		    var y = d.Station.y;
		    return "translate(" + [x,y] + ")";
		})
		.on("mouseover", function(d){
			return tooltip.style("visibility", "visible")
					.html(function() { return trainTimeTable(d)});
		})
		.on("mousemove", function(){
			return tooltip.style("top", (event.pageY-10)+"px").style("left",(event.pageX+10)+"px");
		})
		.on("mouseout", function(){
			return tooltip.style("visibility", "hidden");
		})
		.on("click", function() {
		   var sel = d3.select(this);
  			sel.moveToFront();
		});
		
  
	var circle = groups.append("circle")
		.attr("cy", function(d) { return 0; })
		.attr("cx", function(d) { return 0; })
		.attr("r", function(d) { return 10; })
		.attr("stroke-width", 1)
		.attr("fill",function(d,i){  
			if(d.Direction == 1) return getColor(d.Train.Line);
			return "white";
		})
		.attr("stroke", function(d,i){  
			if(d.Direction == 1) return "black";
			return "black";//getColor(d.Train.Line);
		});
	
	var label = groups.append("text")
	    .text(function(d){
	      return d.Train.DestinationName[0].charAt(0);
	    })
	    .attr("alignment-baseline", "middle")
	    .attr("text-anchor","middle")
	    .attr("fill",function(d){
	    	if(d.Direction == 1) return "white";
			return getColor(d.Train.Line);
	    })
	    .attr("font-family", "cambria")
      
    
}


d3.selection.prototype.moveToFront = function() {
  return this.each(function(){
    this.parentNode.appendChild(this);
  });
};


function getColor(line)
{
	
	if(line == "RD")
	{
		return "red";
	}
	if(line == "BL")
	{
		return "blue";
	}
	if(line == "OR")
	{
		return "orange";
	}
	if(line == "SV")
	{
		return "#c0c0c0";
	}
	if(line == "YL")
	{
		return "#b3b300";
	}
	if(line == "GR")
	{
		return "green";
	}
	
	return "black";
}

function trainTimeTable(d)
{
	var table = '<table ><tr><td>'+d.Train.LocationName+'</td><td class="minRow">'+d.Train.Min+'</td></tr>';

	for (var i = d.OtherTrains.length - 1; i >= 0; i--) 
	{
		table += '<tr><td>'+d.OtherTrains[i].Train.LocationName+'</td><td class="minRow">'+d.OtherTrains[i].Train.Min+'</td></tr>';
	}

	table += '</table>';


	return "<div class=\"toolTip\"><u>"+d.Train.Line+" - "+d.Train.DestinationName+"</u><br><br>"+table+"</div>"; 
}

//get cordinates of click on map 
svg.on("click", function(d) { 

	var coordinates = [0, 0];
	coordinates = d3.mouse(this);
	var x = coordinates[0];
	var y = coordinates[1];

	console.log(x+" , "+y);

})



    
	       
	       










