


function getTrains(data)
{
	var line = sortByTrainLine(data);

	var destination = [];


	for (var i = 0; i < line.length; i++) 
	{
		destination = destination.concat(sortByDestination(line[i].Trains,line[i].Line));
	}
    

	data = sortByTrain(destination);
	return convertToObject(data);

}

function sortByTrainLine(trains)
{
	var red = {
		Line : redLine,
		Trains : [] 
	};

	var blue = {
		Line : blueLine,
		Trains : [] 
	};

	var orange = {
		Line : orangeLine,
		Trains : [] 
	};

	var silver = {
		Line : silverLine,
		Trains : [] 
	};

	var yellow = {
		Line : yellowLine,
		Trains : [] 
	};

	var green = {
		Line : greenLine,
		Trains : [] 
	};


	for (var i = 0; i < trains.length; i++) 
	{
		var element = trains[i]
		if(compareTrainsTime(element.Min, 10) == -1 && element.Min != "")
		{
			if(element.Line === "BL" )
			{
				
				blue.Trains.push(element);
			}

			if(element.Line === "RD" )
			{
				red.Trains.push(element);
			}

			if(element.Line === "OR" )
			{
				orange.Trains.push(element);
			}

			if(element.Line === "SV" )
			{
				silver.Trains.push(element);
			}

			if(element.Line === "YL" )
			{
				yellow.Trains.push(element);
			}

			if(element.Line === "GR" )
			{
				green.Trains.push(element);
			}
		}
	
	}
	

	return [blue, red, orange, silver, yellow, green];

}

function sortByDestination(trains, line)
{
	var commonTrains  = [];

	for (var i = 0; i < trains.length; i++) 
	{
		var element = 
		{ 
			Train: trains[i],
			Station : findStation(trains[i].LocationCode,line)

		};

		var isNotFound = true;
		
		for(var k = 0; k<commonTrains.length;k++)
		{
			var train = commonTrains[k];
			if(train.DestinationCode == element.Train.DestinationCode)
			{
				train.Trains.push(element);
				isNotFound = false;
				break;

			}

		}

		if(isNotFound)
		{
			commonTrains.push({
				DestinationCode : element.Train.DestinationCode,
				DestinationStation: findStation( element.Train.DestinationCode, line),
				Trains : [element]
			});
		}


	}


	return commonTrains;
}

function sortByTrain(commonTrains)
{

    var retArray = [];

	for (var i = 0; i < commonTrains.length; i++) 
	{
		var element = commonTrains[i];

		var direction = 1;
		var index = 0;

		while( index < element.Trains.length-1 && element.Trains[index++].Station.order === element.DestinationStation.order);

		
       
		if(element.DestinationStation.order >= element.Trains[index].Station.order)
		{
			direction = -1;
		}
		
		element.Trains.sort(function(a, b) {

			//sort by station order
			if(a.Station.order > b.Station.order )
			{
				return 1 * direction;
			}
			
			if(a.Station.order < b.Station.order)
			{
				return -1 * direction;
			}

			return compareTrainsTime(a.Train.Min,b.Train.Min);
		});


	
		var carIndex = retArray.length;

		for (var j = 0; j < element.Trains.length-1; j++) 
		{
			var sameStation = [element.Trains[j]];
			var index = 1;

			//Gets all trains at the same station 
			while(j+index < element.Trains.length  && sameStation[0].Station ==  element.Trains[j+index].Station )
			{
				sameStation.push(element.Trains[j+index]);
				index++;
			}



			for (var k = 0; k < sameStation.length; k++) 
			{
				var train = element.Trains[j+k];
				train.Direction = direction;
				

				if(retArray.length == carIndex+k)
				{
					retArray.push([train]);
				}
				else
				{
					var item = retArray[carIndex+k][retArray[carIndex+k].length-1];

					if(compareTrainsTime( item.Train.Min, train.Train.Min) === 1)
					{
						retArray[carIndex+k].push(train);

					}
					else
					{
						carIndex++;

						if(retArray.length == carIndex+k)
						{
							retArray.push([train]);
						}
						else
						{
							retArray[carIndex+k].push(train);
						}

					}
					
				}
			}

			j = j+index-1;

		}	

	}

	return  retArray;
	
}

function convertToObject(trains)
{
	var retArray = [];
	for (var i = 0; i<trains.length; i++) 
	{   
		var element = trains[i].pop();
		console.log(element );
		element.OtherTrains = trains[i];
		//element.Station = findStatonFull(element);

		retArray.push(element);
	}

	return retArray;
}


function compareTrainsTime(a,b)
{
	//If same station sort by minutes  from station
	if(a == "BRD" )
	{
		return -1;
	}

	if(b == "BRD")
	{
		return 1;
	}

	if(a == "ARR" )
	{
		return -1;
	}

	if(b == "ARR" )
	{
		return 1;
	}

	if(Number(a) > Number(b))
	{
		return 1;
	}

	return -1;
}

function findStatonFull(train)
{

	if(train.Train.Min == "ARR" || train.Train.Min == "BRD" )
	{
		return train.Station;
	}	

	var index = train.Station.Index + train.Direction * Number(train.Train.Min);

	if(index < 0)
	{
		index = 0;
	}

	if(index > blueLineFull.length - 1)
	{
		index = blueLineFull.length - 1;
	}

	return blueLineFull[index];

}


function findStation(locationCode, line)
{

	for(var j = 0; j< line.length;j++)
	{
		var train = line[j];
		if(train.DestinationCode == locationCode)
		{
			train.Index = j;
			return train;
		
		}

	}

	return undefined; 

}






